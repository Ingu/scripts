#!/bin/bash
sudo apt-get -y install asciidoc xmlto docbook2x
sudo apt-get -y install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev
git_ver=2.8.2
wget https://github.com/git/git/archive/v$git_ver.tar.gz -O /tmp/git_download.tar.gz
tar -zxf /tmp/git_download.tar.gz -C /tmp
cd /tmp/git-$git_ver
make configure
./configure --prefix=/usr
make all doc info
sudo make install install-doc install-html install-info